﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EncapsuleMe
{
    public class Ouf
    {
        #region private attributs
        //passer les attributs en private
        private int laf;//supprimer l'initialisation
        private float oursk;
        private string mixtesas;
        #endregion private attributs

        #region constructors
        public Ouf(int aslk, float vlugrp, string akalk)
        {
            //utiliser les variables passées en paramètres pour initialiser les attributs privés
            this.laf += aslk;
            this.oursk = vlugrp;
            this.mixtesas = akalk;
        }

        #endregion constructors

        #region public accessors and mutators
        //créer les accesseurs
        public int Laf { get => laf;}
        public float Oursk { get => oursk;}
        public string Mixtesas { get => mixtesas;}
        #endregion public accessors and mutators
    }
}
